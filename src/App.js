import {useState, useEffect} from 'react'
import {Container} from 'react-bootstrap'
import {BrowserRouter, Switch, Route} from 'react-router-dom';

import 'bootstrap/dist/css/bootstrap.min.css'; 

//for React Context
import { UserProvider } from './UserContext';

/*components*/
import Navigation from './components/Navigation';
import Login from './components/Login';
import Logout from './components/Logout';
import ErrorPage from './components/Error';
import Register from './components/Register';
/*pages*/
import Home from './pages/Home';
import Courses from './pages/Courses';
import CourseView from './pages/CourseView';


export default function App() {
    let token = localStorage.getItem("token")

    //global state
    const [user, setUser] = useState({
              id: null,
              isAdmin: null
          })

    //function for clearing localStorage on logout
    const unsetUser = () => {
          localStorage.clear();

          setUser({
            id: null,
            isAdmin: null
          })
    }


    useEffect( () => {

      fetch("http://localhost:3000/api/users/details", {
            headers: {
              "Authorization": `Bearer ${token}`
            }
      })
      .then(response => response.json())
      .then(data => {
        // console.log(data !== "undefined")
          if(typeof data._id !== "undefined"){
            // console.log("if codeblock")
            setUser({
              id: data._id,
              isAdmin: data.isAdmin
            })
          }
      })
    }, [])

  return(
    <UserProvider value={{ user, setUser, unsetUser}}>
      <BrowserRouter>
        <Navigation/>
          <Container fluid className="m-3">
            <Switch>
              <Route exact path="/" component={Home} />
              <Route exact path="/courses" component={Courses} />
              <Route exact path="/courses/:courseId" component={CourseView} />
              <Route exact path="/register" component={Register} />
              <Route exact path="/login" component={Login} />
              <Route exact path="/logout" component={Logout} />
              <Route component={ErrorPage} />
            </Switch>
          </Container>
      </BrowserRouter>
    </UserProvider>
    )
} 
