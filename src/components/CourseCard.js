import {useState, useEffect} from "react";
import {
    Card,
    Button,
    Col,
    Row,
    Container
} from "react-bootstrap"
import PropTypes from "prop-types";


export default function CourseTemplate ({courseProp}) {
    const {name, description, price} = courseProp

    // use hook to store its state
    // states to keep track of info related to individual components
    // syntax const[getter, setter] = useState(initialGetterValue)

    const [count, setCount] = useState (0)
    const [seat, availableSeat] = useState (10)
    const [isDisabled, setIsDisabled] = useState(false)

    function countEvent () {

        if (count === 10) {
            console.log (`${name} Enrollees: ${count}`)
            setCount (count)
        } else {
            setCount (count + 1)
            console.log (`${name} Enrollees: ${
                count + 1
            }`)
        }


        if (seat === 0) {
            availableSeat (seat)
            console.log (`Available seats: ${seat}`)
            alert (`There are no more available seats for the ${name} class`)
        } else {
            availableSeat (seat - 1)
            console.log (`Available seats: ${
                seat - 1
            }`)
        }
    }

    useEffect( () => {
        if(seat === 0){
            setIsDisabled(true)
        }
    }, [seat])
    
    return (<Container fluid className="my-5">
        <Row className="justify-content-center">
            <Col>
                <Card className="p-4">
                    <Card.Body>
                        <Card.Title> {name}</Card.Title>
                        <Card.Subtitle>Description</Card.Subtitle>
                        <Card.Text> {description} </Card.Text>
                        <Card.Subtitle>Price</Card.Subtitle>
                        <Card.Text>PhP {price}</Card.Text>
                        <Card.Subtitle>Enrollees</Card.Subtitle>
                        <Card.Text>{count} enrolled students</Card.Text>
                        <Button variant="primary"
                            onClick={countEvent} disabled={isDisabled}>Enroll</Button>
                    </Card.Body>
                </Card>
            </Col>
        </Row>
    </Container>)
}

// proptypes are used to validate information passed to a component

CourseTemplate.propTypes = { // shape method, check if object conforms to a specific shape
    courseProp: PropTypes.shape (
        { // define properties
            name: PropTypes.string.isRequired,
            description: PropTypes.string.isRequired,
            price: PropTypes.number.isRequired
        }
    )
}
